<?php
return [
    'email' => 'Email',
    'phone' => 'Phone',
    'password' => 'Password',
    'int' => 'Integer',
    'antispam' => 'Antispam',
];