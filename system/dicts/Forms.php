<?php
 return [
     'login' => [
         'template' => 'login',
         'backend' => 'login',
         'fields' => [
             'login' => ['extends'=>'login'],
             'password' => ['extends'=>'password'],
         ]
     ],


	 'signup' => [
         'fields' => [
             'name' => ['extends'=>'name'],
             'login' => ['extends'=>'login'],
             'password' => ['extends'=>'password'],
             'secpassword' => [
                 'extends'=>'password',
                 'title' => 'Подтвердите пароль',
                 'help_text' => '',
             ],
         ]
     ],
];