<?php
    return  [
 			'name' => [
                    'title' => 'ФИО',
                    'help_text' => 'Вводите настоящие Имя, Фамилию и Отчество',
                    'type' => 'text',
                    'required' => true,
                    'rule' => 'noemty',
                    'attrs' => [
                        'placeholder'=> 'Иванов Иван Иванович',
                    ],
            ],

			'email' => [
                    'title' => 'Email',
                    'help_text' => 'Вводите настоящий Email',
                    'type' => 'text',
                    'required' => true,
                    'rule' => 'email',
                    'attrs' => [
                        'placeholder'=> 'Введите ваш адрес электронной почты',
                    ],
            ],
			'password' => [
                    'title' => 'Пароль',
                    'help_text' => 'Пароль должен состоять из ...',
                    'type' => 'password',
                    'rule' => 'password',
                    'required' => true,
            ],

			'login' => [
                    'title' => 'Логин',
                    'help_text' => 'Email или телефон',
                    'type' => 'text',
                    'rule' => 'login',
                    'required' => true,
                    'attrs' => [
                        'placeholder'=> 'Email или Телефон',
                    ],
            ],

            'text' => [
                    'title' => 'Текст',
                    'help_text' => 'Какое то текстовое поле',
                    'type' => 'textarea',
                    'rule' => 'login',
                    'required' => true,
                    'attrs' => [
                        'placeholder'=> 'Введите текст сообщения',
                    ],
            ],

            'phone' => [
                    'title' => 'Телефон',
                    'help_text' => '79999999999, 777777',
                    'type' => 'text',
                    'rule' => 'phone',
                    'required' => true,
                    'attrs' => [
                        'placeholder'=> 'Мой телефон',
                    ],
            ],

			'file' => [
                    'title' => 'Файл',
                    'type' => 'file',
            ],

            'hidden' => [
                    'title' => 'Скрытое поле',
                    'type' => 'hidden',
            ],

            'antispam' => [
                    'title' => 'Имя',
                    'type' => 'text',
                    'rule' => 'antispam',
                    'attrs' => [
                        'style' => 'display:none',
                    ]
            ]
];