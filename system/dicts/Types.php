<?php
return [
        'buttons' => [
            'submit' => 'Submit',
            'button' => 'Button',
            'reset' => 'Cancel',
        ],
        'inputs' => [
            'text' => 'Text',
            'email' => 'Email',
            'phone' => 'Phone',
            'hidden' => 'Hidden',
            'password' => 'Password',
            'checkbox' => 'Checkbox',
            'radio' => 'Radio',
            'file' => 'File',
        ],
        'other' => [
            'select' => 'Select',
            'textarea' => 'Textarea',
        ]
    ];