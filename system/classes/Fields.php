<?php


class Fields {



    public static function submit($text = "Отправить", $class="btn btn-default") {
        return '<button type="submit" class="'.$class.'">'.$text.'</button>';
    }


    public static function email ($name, $field) {
        return self::input($name, $field);
    }

    public static function password ($name, $field) {
        return self::input($name, $field);
    }

    public static function phone ($name, $field) {
        return self::input($name, $field);
    }
    public static function checkbox ($name, $field) {
        return self::input($name, $field);
    }
    public static function radio ($name, $field) {
        return self::input($name, $field);
    }
    public static function text ($name, $field) {
        return self::input($name, $field);
    }
    public static function hidden ($name, $field) {
        return self::input($name, $field);
    }

    public static function input($name, $field) {
        if (!isset($field['values'])) {
            $res = '<input type="' . $field['type'] . '" name="' . $name . '" ';

            if (isset($field['attrs'])) {
                foreach ($field['attrs'] as $attr => $value) {
                    $res .= $attr . '="' . $value . '" ';
                }
            }
            if (empty($field['id']) && empty($field['attrs']['id'])) {
                $val = isset($field['value']) ? $field['value'] : '';
                $fieldid = str_replace('[]', '', $name);
                $fieldid .= $val;
                $res .= " id=$fieldid ";
            }

            if (isset($field['required']) && $field['type'] != 'checkbox' && $field['type'] != 'radio') {
                $res .= ' required ';
            }
            if (isset($field['value'])) {
                $res .= 'value="' . $field['value'] . '"';
            }
            $res .= ' />';


        } else {
            $res = '';
            foreach ($field['values'] as $val=>$title) {
                if (!empty($title)){
                    $res .= '<label>';
                }
                $res .= '<input type="' . $field['type'] . '" name="' . $name . '" ';

                if (isset($field['attrs'])) {
                    foreach ($field['attrs'] as $attr => $value) {
                        $res .= $attr . '="' . $value . '" ';
                    }
                }
                if (empty($field['id']) && empty($field['attrs']['id'])) {

                    $fieldid = str_replace('[]', '', $name);
                    $fieldid .= $val;
                    $res .= " id=$fieldid ";
                }

                if (isset($field['required']) && $field['type'] != 'checkbox' && $field['type'] != 'radio') {
                    $res .= ' required ';
                }
                if (isset($field['value']) && $field['value'] == $val) {
                    $res .= ' checked ';
                }
                $res .= ' />';
                if (!empty($title)){
                    $res .= ' '.$title;
                    $res .= '</label>';
                }
            }
        }
        return $res;
    }

    public static function select($name, $field) {
        $res = '<select name="' . $name . '"';
        if (isset($field['attrs'])) {
            foreach ($field['attrs'] as $attr => $value) {
                $res .= $attr . '="' . $value . '" ';
            }
        }
        $res .= ">";
        if (isset($field['values'])) {
            foreach ($field['values'] as $key => $value) {
                if (isset($value['group'])) {
                    $res .= '<optgroup label="' . $value['title']  . '">';
                    foreach ($value['group'] as $key => $value) {
                        $res .= '<option value="' . $key . '"';
                        if (isset($field['value']) && $key == $field['value']) {
                            $res .= 'selected="selected"';
                        } elseif (isset($value['current']) && !isset($field['value'])) {
                            $res .= 'selected="selected"';
                        }
                        if (isset($value['attrs'])) {
                            foreach ($value['attrs'] as $k => $v) {
                                $res .= ' ' . $k . '="' . $v . '"';
                            }
                        }
                        $res .= '>';
                        $res .= $value['title'];
                        $res .= "</option>";
                    }
                    $res .= "</optgroup>";
                } else {
                    $res .= '<option value="' . $key . '"';

                    if (isset($field['value']) && $key == $field['value']) {
                        $res .= 'selected="selected"';
                    } elseif (isset($value['current']) && !isset($field['value'])) {
                        $res .= 'selected="selected"';
                    }
                    if (isset($value['attrs'])) {
                        foreach ($value['attrs'] as $k => $v) {
                            $res .= ' ' . $k . '="' . $v . '"';
                        }
                    }

                    $res .= '>';
                    $res .= $value['title'];
                    $res .= "</option>";
                }
            }
        }

        $res .= '</select>';
        return $res;
    }

    public static function textarea ($name, $field) {
        $res = '<textarea name="' . $name . '"';
        if (isset($field['attrs'])) {
            foreach ($field['attrs'] as $attr => $value) {
                $res .= $attr . '="' . $value . '" ';
            }
        }
        if ($field['required']) {
            $res .= ' required ';
        }
        $res .= ">";
        if (isset($field['value'])) {
            $res .= $field['value'];
        }

        $res .= '</textarea>';
        return $res;
    }
}