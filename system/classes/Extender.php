<?php

class Extender {



    public static function run($form) {

        $form = self::extendsForm($form);
        foreach ($form['fields'] as $key=>&$field) {
            $field = self::extendsField($field);
        }

        return $form;
    }

    private function extendsField($field) {
        $presets = require DICT_PATH . '/Fields.php';
        $default_field = [];

        if (isset($field['extends']) && isset($presets[$field['extends']])) $default_field = $presets[$field['extends']];
        if (isset($form['extends'])) unset($form['extends']);


        foreach ($field as $attr => $val) {
            if ($attr == 'attrs') {
                foreach ($val as $key => $value) {
                    if (!isset($default_field['attrs'])) $default_field['attrs'] = [];
                    $default_field['attrs'][$key] = $value;
                }
            } else {
                $default_field[$attr] = $val;
            }
        }
        return $default_field;
    }
    private function extendsForm($form) {
        $presets = require DICT_PATH . '/Forms.php';
        $default_form = [];

        if (isset($form['extends']) && isset($presets[$form['extends']])) $default_form = $presets[$form['extends']];
        if (isset($form['extends'])) unset($form['extends']);


        if (!isset($default_form['fields'])) $default_form['fields'] = [];
        foreach ($form as $param=>$value) {
            if ($param == 'fields') {
                foreach ($value as $key=>$field) {
                    $default_form['fields'][$key] = $field;
                }
            } else {
                $default_form[$param] = $value;
            }
        }
        return $default_form;
    }
}