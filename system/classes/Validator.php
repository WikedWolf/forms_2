<?php


class Validator {

     private static $errors = [];

    public static function run($fields) {
        foreach ($fields as $key => $field) {
            if (isset($field['required'])) {
                self::required($key, $field['value']);
            }
            if (isset($field['rule']) && method_exists((new Validator()), $field['rule'])) {
                self::$field['rule']($key, $field['value']);
            }
        }
        if (self::$errors) {
            return self::$errors;
        } else {
            return true;
        }
    }


    private function required($key, $value) {
        if (!$value) {
            self::$errors[$key] = 'empty';
        }

    }

    private function email($key, $value) {
        $match = preg_match('#^(\d|[a-zA-Z0-9_\-]|\.)+@(\d|[a-zA-Z0-9_\-\.].+)\.([a-zA-Z_\-]){2,4}$#', $value);
        $match = $match == 1 ? true : false;
        if (!$match) {
            self::$errors[$key] = 'notmatch';
        }
    }


    private function phone($key, $value) {
        $len_before = strlen($value);
        $value = preg_replace('/[^0-9]/', '', $value);
        $len_after = strlen($value);

        if (($len_after == 0) && ($len_before > 0)) {
            self::$errors[$key] = 'notmatch';
        } elseif ($len_after > 5 && $len_after < 13) {

        } elseif (($len_after <= 5) && ($len_after > 0)) {
            self::$errors[$key] = 'small';
        } elseif ($len_after > 0) {
            self::$errors[$key] = 'big';
        }

    }

    private function password($key, $value) {
        if (strlen($value) <= 5) {
            self::$errors[$key] = 'small';
        }
    }

    private function int($key, $value) {
        $match = preg_match('#^\d+$#', $value);
        $match = $match == 1 ? true : false;
        if (!$match) {
            self::$errors[$key] = 'notmatch';
        }
    }

    private function antispam($key, $value) {
        $len = strlen($value);
        if ($len) {
            self::$errors[$key] = 'notmatch';
        }
    }
}