<?php

class Response {

    public static function render ($input) {
        if (is_array($input)) {
            return self::renderJson($input);
        }else {
            echo $input;
            die();
        }
    }
    private function renderJson($input) {
        header('Content-Type: application/json;  charset=utf-8');
        echo json_encode($input);
        die;
    }
}