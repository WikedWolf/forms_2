<?php

class Cacher {

    public static function cache($array = false) {
        $settings = new Settings();
        $file = $settings['folders']['cache']. $array['form']['id'] . '.cache';
        $str_value = serialize($array);

        $f = fopen($file, 'w');
        fwrite($f, $str_value);
        fclose($f);

    }

    public static function uncache($formname) {
        $settings = new Settings();
        $file = $settings['folders']['cache'].'/'.$formname . '.cache';
        $file = file_get_contents($file);
        $value = unserialize($file);

        return $value;
    }
}