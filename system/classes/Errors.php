<?php


class Errors {
    private static $errors = [];



    public static function add($validate, $form) {
        self::$errors = require DICT_PATH . '/Errors.php';

        foreach ($validate as $key=>$val) {
            $form['fields'][$key]['error'] = self::$errors[$val];
        }
        return $form;
    }

    public static function flush($errors) {

        self::$errors = require DICT_PATH . '/Errors.php';
        if (is_array($errors)) {
            foreach ($errors as $key => &$value) {
                $value = [$value => self::$errors[$value]];
            }

            return Response::render($errors);
        }

    }
}