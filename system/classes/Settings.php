<?php

/**
 * Created by PhpStorm.
 * User: wikedwolf
 * Date: 18.12.15
 * Time: 10:20
 */
class Settings implements ArrayAccess {
    private $container = [];

    public function __construct() {
        $file = CONFIG_DIR . '/config.yml';
        if (!file_exists($file)) {
            print '<strong>Ошибка</strong>: Файл настоек не найден (config/config.yml)';
        }
        $settings = $this->params = YamlLoader::run($file);

        $this->container = $settings;
    }

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->container[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->container[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }
}