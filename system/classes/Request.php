<?php


class Request {

    public function get($var) {
        return isset($_REQUEST[$var]) ? $_REQUEST[$var] : false;
    }

    public function submitted($form_id) {
        if ($this->get('form_id') && $this->get('form_id') == $form_id) return true;
        else return false;
    }

    public function getData () {
        return $_REQUEST;
    }

}