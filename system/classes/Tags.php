<?php
class Tags {


    public static function form($form) {
        $settings = new Settings();
        $yandex = '';
        if (isset($form['yandex'])){
            $goal = str_replace(' ', '', $form['yandex']['goal']);
            $yandex = 'onsubmit="yaCounter'.$form['yandex']['counter'].'.reachGoal(\''.$goal.'\'); return true;"';
        }
        if(isset($form['on_ajax']) && $form['on_ajax']){
            $ajax = 'class="on_ajax"';
            $url = $settings['urls']['handler'];
        }else{
            $url = '.';
        }
        $res = '<form action="'.$url.'" '.$ajax.' '.$yandex.' method="post">';
        $res .= '<input type="hidden" name="form_id" value="' . $form['id'] . '">';
        return $res;

    }

    public static function endform($form) {
        return '</form>';
    }


}