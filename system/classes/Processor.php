<?php
/*
 * Processor light v1.0.0
 */
ini_set("display_errors", 1);
class Processor {
    private static $template;
    private static $form;
    private static $tempvars = array();
    private static $settings;
    public static function run($form) {
        self::$settings = new Settings();
        if (!isset($form['template'])) echo '<strong>Ошибка:</strong> В настройках формы не указан шаблон!';
        $template = file_get_contents(MROOT .'/'. self::$settings['folders']['template'] . $form['template'] . '.html');
        $template_as_string = preg_replace('/[\r\n]+(?![^(]*\))/', "", $template);
        $template_as_string = str_replace('  ', ' ', $template_as_string);
        self::$template = $template_as_string;
        self::extendsTemplate();
        self::$form = $form;
        self::findTags();
        self::$template = self::replaceVars(self::$template);
        return self::$template;
    }


    private function extendsTemplate() {

        $template = self::$template;
        preg_match('#\{\%\s+extends\s([a-zA-Z0-9_-]+)#', self::$template, $ext);

        if ($ext){
            $tplname = $ext[1];
            $path = MROOT .'/'. self::$settings['folders']['template'] . $tplname . '.html';
            $template = file_get_contents($path);

            $tpls = explode('{% endblock %}', self::$template);
            $blocks = [];
            $blockNames = [];
            foreach ($tpls as &$tpl){

                $block = preg_match('#{%\sblock#', $tpl, $match);
                if ($match){
                    $tpl .= "{% endblock %}";
                }

                preg_match('#\{\%\s+block\s([0-9a-zA-Z\s_\-\,\.\=]+)\%\}(.+)\{\%\s+endblock\s+\%\}#s', $tpl, $tplblocks);
                if (isset($tplblocks[0])){
                    $blocks[] = $tplblocks[2];
                    $blockNames[] = $tplblocks[1];
                }




            }

            foreach ($blockNames as $key=>$val){
                $val = trim($val);
                $regex = '#\{\%\s+block\s+'.$val.'\s+\%\}\{\%\s+endblock\s+\%\}#';

                $template = preg_replace($regex, $blocks[$key], $template);

            }

            self::$template = $template;






        }
    }


    private function findTags () {

        preg_match_all('#\{\%\s+for\s([0-9a-zA-Z\s_\-\,\.\=]+)\%\}(.*)\{\%\s+endfor\s+\%\}#s', self::$template, $tplfor);

        foreach ($tplfor[0] as $key=>$for) {
            $ex = $tplfor[1][$key];
            $code = $tplfor[2][$key];
            $res = '';
            $ex = self::parseForEx($ex);
            preg_match_all('#\{\%([0-9a-zA-Z\s_\-]+)\%\}#', $code, $matches);
            $var = $ex['var'];
            $exVars = explode('=>', $ex['as']);

            $execute = 'foreach($var as '.$ex['as'].'){
                $codedub = $code;

                if (count($exVars) > 1){
                    self::$tempvars[\''.$exVars[0].'\'] = '.$exVars[0].';

                    self::$tempvars[\''.$exVars[1].'\'] = '.$exVars[1].';
                }else {
                    self::$tempvars[\''.$exVars[0].'\'] = '.$exVars[0].';
                }


                $codedub = self::replaceIf($codedub);

                preg_match_all(\'#\{\%([0-9a-zA-Z\s_\-]+)\%\}#\', $codedub, $tags);
                $codedub = self::replaceTags($tags, $codedub);
                $codedub = self::replaceVars($codedub);

                $res .= $codedub;

                }';


            eval($execute );



            self::$template = str_replace($for, $res, self::$template);


        }
        preg_match_all('#\{\%([0-9a-zA-Z\s_\-]+)\%\}#', self::$template, $matches);
        self::$template = self::replaceTags($matches, self::$template);


    }

    private function replaceIf($tpl){

        preg_match_all('#\{\%\s+if\s([0-9a-zA-Z\s_\-\,\.\=\>\<\'\!]+)\%\}(.*)\{\%\s+endif\s+\%\}#s', $tpl, $tplif);

        foreach ($tplif[0] as $key=>$if){
            $keyif = $key;
            $ex = $tplif[1][$key];

            $orandex = preg_split('#(and|or)+#',  $ex, 4 ,PREG_SPLIT_DELIM_CAPTURE);

            foreach ($orandex as $key=>&$value) {
                if ($value != 'and' && $value != 'or') {
                    preg_match('#(\!\=|\!|>=|<=|>|<|==)#', $value, $match);
                    $res = 0;
                    if ($match[0]) {
                        list($first, $second) = explode($match[0], $value);
                        $first = self::replaceVar($first);
                        $second = self::replaceVar($second);

                        eval('if ('.$first.' '.$match[0].' '.$second.') $res = 1;');

                    }else{
                        $one =  self::replaceVar($value);
                        eval('if ('.$one.') $res = 1;' );
                    }
                    $value = $res;

                }
            }
            $resif = 0;
            foreach ($orandex as $key=>$val) {
                if ($val > 0){
                    if ($key>0) {
                        if ($orandex[$key-1] == 'and'){
                            if ($orandex[$key-2] >0){
                                $res = 1;
                            }else{
                                $res = 0;
                            }
                        }else{
                            $res = 1;
                        }
                    }else{
                        $res = 1;
                    }
                }else{
                    $res = 0;
                }
            }


            $code = $tplif[2][$keyif];

            if ($res == 1){
                $tpl = str_replace($tplif[0][$keyif], $code, $tpl );
            }else{
                $tpl = str_replace($tplif[0][$keyif], '', $tpl );
            }

        }
        return $tpl;
    }


    private function parseForEx($ex) {
        list($as, $var) = explode(' in ', $ex);
        $res = array();
        $as = explode(',', $as);
        $var = self::replaceVar($var);
        $res['var'] = $var;
        if (count($as) > 1) {
            $as[1] = trim($as[1]);
            $as[0] = trim($as[0]);
            $as = '$'.$as[0].'=>$'.$as[1];
        }else{
            $as = '$'.$as[0];
        }
        $res['as'] = $as;
        return $res;
    }

    private function replaceTags($matches, $tpl) {
        foreach ($matches[0] as $key=>$value) {
            $tag = trim($matches[1][$key]);
            $tpl = str_replace($value, Tags::{$tag}(self::$form), $tpl);

        }

        return $tpl;
    }

    private function replaceVar($input) {
        list($field, $var) = explode('.', $input);
        $var = trim($var);
        $field = trim($field);
        if ($var){
            $res = isset(self::$form['fields'][$field][$var]) ? self::$form['fields'][$field][$var] : $input;
        }else{
            $res = isset(self::$tempvars[$input]) ? self::$tempvars[$input] : $input;
        }


        return  $res;
    }

    private function replaceVars($tpl) {

        preg_match_all('#\{\{([0-9a-zA-Z\s_]+)\.([0-9a-zA-Z\s_]+)\}\}#', $tpl, $vars);
        preg_match_all('#\{\{([0-9a-zA-Z\s_]+)\}\}#', $tpl, $fields);

        foreach ($vars[0] as $key=>$value) {
            $field = trim($vars[1][$key]);
            $var = trim($vars[2][$key]);
            $repl = isset(self::$form['fields'][$field][$var]) ? self::$form['fields'][$field][$var] : '';
            if (!$repl){
                $repl = isset(self::$tempvars['$'.$field][$var]) ? self::$tempvars['$'.$field][$var] : '';


            }

            $tpl = str_replace($value, $repl, $tpl);
        }


        foreach ($fields[0] as $key=>$value) {
            $field = trim($fields[1][$key]);
            $params = [];

            if ($field == 'submit') {
                $type = 'submit';
            }

            if (isset(self::$form['fields'][$field])) {
                $params = self::$form['fields'][$field];
                $type = $params['type'];
            }

            if ($type && method_exists((new Fields()), $type)) {
                if ($type == 'submit') $tpl = str_replace($value, Fields::{$type}(), $tpl);
                else $tpl = str_replace($value, Fields::{$type}($field, $params), $tpl);
            }else{

                $tpl = str_replace($value, self::$tempvars['$'.$field], $tpl);
            }

        }
        return $tpl;
    }

}