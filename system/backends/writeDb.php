<?php






class writeDb {

    public static function run($params, $form) {
        $mysql = new Mysql();
        $mysql->devMode = 1;
        $table = $params['db']['table'];

        foreach ($form->fields as $id => $field) {
            if ($field['value'] && is_array($field['value'])) {
                $values = [];
                foreach ($field['value'] as $val) {
                    $values[] = $val;
                }
                $field['value'] = serialize($values);
            }

            $mysql->add($id, $field['value']);

        }

        $res = $mysql->insert($table);
        return array($params, $form);
    }


}