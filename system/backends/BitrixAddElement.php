<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
/**
 * Created by PhpStorm.
 * User: wikedwolf
 * Date: 22.12.15
 * Time: 9:31
 */
class BitrixAddElement {
    public static function run($params, $form){

        CModule::IncludeModule('iblock');
        $el = new CIBlockElement;
        $prop = [];
        $arLoadProductArray = [
            "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
            "IBLOCK_ID" => $params['iblock'],
            "ACTIVE" => "Y",            // активен
            "ACTIVE_FROM" => date('d.m.Y'),
        ];
        if ($disactive) {
            $arLoadProductArray['ACTIVE'] = 'N';
        }
        foreach ($form['fields'] as $id => $field) {
            list($pr, $name) = explode('.', $id);
            if ($pr == 'PROPERTY') {
                $prop[$name] = $field['value'];
            } else {
                if ($field['type'] == 'file') {



                    $arLoadProductArray[$id] = $_FILES[$id];
                } else {
                    $arLoadProductArray[$id] = $field['value'];
                }

            }
        }
        if (!isset($arLoadProductArray['CODE'])) {
            $arLoadProductArray['CODE'] = Translite::run($arLoadProductArray['NAME']);
        }
        $arLoadProductArray['PROPERTY_VALUES'] = $prop;
        $el->Add($arLoadProductArray);
        return array($params, $form);
    }
}