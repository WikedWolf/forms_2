<?php


function __autoload($class) {
    $user_backends = MROOT . '/backends/' . $class . '.php';
    $standard_backends = MROOT . '/system/backends/' . $class . '.php';
    $calsspath = MROOT . '/system/classes/' . $class . '.php';
    $utilpath = MROOT . '/system/utils/' . $class . '/' . $class . '.php';
    if (file_exists($calsspath)) {
        require_once $calsspath;
    } elseif (file_exists($utilpath)) {
        require_once $utilpath;
    } elseif (file_exists($standard_backends)) {
        require_once $standard_backends;
    }  elseif (file_exists($user_backends)) {
        require_once $user_backends;
    } else {
        print '<strong>Ошибка:</strong> Класс ' . $class . ' не найден';
    }
}