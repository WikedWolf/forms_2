<?php

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//////###////###////###//////###////###////###////////#///#####////
////////#////#/#///#///////////#////#/#///#//////////##///#////////
/////////#/#////#/#/////////////#/#////#/#////////////#///#####////
//////////#//////#///////////////#//////#/////////////#///////#////
/////////###////###/////////////###////###////////////#///#####////
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//                                                               //
//   Этот модуль предназначен для построения и работы с формами. //
//                                                               //
//                                                   WW 2015.    //
//   Version 3.0.1                                               //
//   Requires >= PHP 5.4.x                                         //
//   LAST UPD 18.12.15                                           //
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

define('MROOT', __DIR__);
define('CONFIG_DIR', MROOT . '/config');
define('SYSTEM_ROOT', MROOT . '/system');
define('DICT_PATH', SYSTEM_ROOT . '/dicts');

require_once CONFIG_DIR . '/loader.php';


class Forms {


    private $request;
    private $params;
    private $form;
    protected $settings;
    private $ajax;

    public function __construct($params = false) {
        $settings = new Settings();
        $this->request = new Request();
        $this->settings = $settings;
        $this->ajax = true;

        if ($this->request->get('form_id')) {
            $form_id = $this->request->get('form_id');
            $this->params = Cacher::uncache($form_id);
        }
        elseif ($params) {
            $file = $this->settings['folders']['forms'] . $params . '.yml';
            $this->params = YamlLoader::run($file);
            Cacher::cache($this->params);
        }

        return $this->run();
    }

    public function run() {

        $this->form = Extender::run($this->params['form']);

        if ($this->request->submitted($this->form['id'])) {
            $this->prepare();

            $validate = Validator::run($this->form['fields']);
            $this->form = Errors::add($validate, $this->form);

            $this->run_backends();

        }
        $html = Processor::run($this->form);
        Response::render($html);
    }


    private function prepare() {

        $data = $this->request->getData();


        foreach ($this->form['fields'] as $key => $value) {


            $this->form['fields'][$key]['value'] = isset($data[str_replace('.', '_', $key)]) ? $data[str_replace('.', '_', $key)] : '';
        }


    }

    private function run_backends()
    {
        $customBackends = new Custom();

        if ($this->params['backends']['before']) {
            if (method_exists($customBackends, $this->params['backends']['before'])) {
                list($params, $form) = $customBackends->{$this->params['backends']['before']}($this->params, $this->form);
                if ($params) $this->params = $params;
                if ($form) $this->params = $form;
            }
        }

        if ($this->params['backends']['backend']) {

            if (class_exists($this->params['backends']['backend'])) {
                list($params, $form) = call_user_func($this->params['backends']['backend'].'::run', [$params, $form]);
            } elseif (method_exists($customBackends, $this->params['backends']['backend'])) {
                list($params, $form) = call_user_func($this->params['backends']['backend'].'::run', [$params, $form]);
            }
            if (isset($params)) $this->params = $params;
            if (isset($form)) $this->params = $form;
        }

        if ($this->params['email']) {
            SendEmail::run($this->params['email'], $this->form);
        }
        if ($this->params['sms']) {
            SendSms::run($this->params['email'], $this->form);
        }


        if ($this->params['backends']['after']) {
            if (method_exists($customBackends, $this->params['backends']['after'])) {
                list($params, $form) = $customBackends->{$this->params['backends']['after']}($this->params, $this->form);
                if ($params) $this->params = $params;
                if ($form) $this->params = $form;
            }
        }
    }
}