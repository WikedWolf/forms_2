/**
 * Created by wikedwolf on 17.03.16.
 */
$(document).ready(function(){
    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/monokai");
    editor.getSession().setMode("ace/mode/twig");
    $('.thumbler').on('change', function () {
        if ($(this).is(':checked')){
            $('#'+$(this).attr('id')+'Box').slideDown();
        }else{
            $('#'+$(this).attr('id')+'Box').slideUp();
        }

    })





    $('.addTemplate').on('click', function(){
        editor.setValue("{% form %}\n\n<!--\n Сюда пишем верстку начиная, которая идет после тега <form>.\n " +
            "Для вывода поля используйте {{ <Имя> }}. \n Для вывода параметра поля используйте {{<Имя>.<Параметр>}}\n " +
            "Поддерживаются логическая функция {% if <Условие> %}...{% endif %} \n " +
            "Поддерживается цикл {% for [<Ключ>,] <Значение>  in <Переменная> %}, где переменная, например values у select (mysel.values) \n " +
            "!!!ВНИМАНИЕ!!! PHP внутри шаблонов не поддерживается!\n -->\n\n  {{submit}}\n{% endform %}");
        $(this).hide();
        $('#newTemplate').slideDown();
        $('.templateSelect').slideUp();
        return false;
    })

    $('#addField').on('click', function() {

        $.ajax({
            type:'POST',
            url: 'tools/get_field.php',
            success:function(output){
               $('#formFields').append(output);
                $.extendsField();
            }
        })


        return false;


    })
    $('#addField').click();

    $("#extendsForm").on('change', function(){
        data = {form_id:$(this).val()}
        $.ajax({
            type:'POST',
            url: 'tools/get_form.php',
            dataType: 'json',
            data: data,
            success:function(output){
                $('#formFields').html(output.fields);
                $.extendsField();
            }
        })

    })
    $('#templateName').on('focus', function() {
        $(this).parent().removeClass('has-error');
    })

    $('#saveTemplate').on('click', function(){
        if (!$('#templateName').val()){
            $('#templateName').parent().addClass('has-error');
            return false;
        }else{
            $('#templateName').parent().removeClass('has-error')
        }

        data = {
            template_name: $('#templateName').val(),
            template_code: editor.getValue()

        }
        $.ajax({
            type:'POST',
            url: 'tools/save_template.php',

            data: data,
            success:function(){

                editor.setValue('');
                tpl = $('#templateName').val();
                $('#templateName').val('');
                $('.templateSelect').append($("<option></option>")
                    .attr("value",tpl)
                    .attr("selected",'selected')
                    .text(tpl)); ;
                $('.templateSelect').slideDown();
                $('#newTemplate').slideUp();
                $('.addTemplate').show();



            }
        })
        return false;
    })
    $('#clearTemplate').on('click', function(){
        editor.setValue("{% form %}\n\n<!--\n Сюда пишем верстку начиная, которая идет после тега <form>.\n " +
            "Для вывода поля используйте {{ <Имя> }}. \n Для вывода параметра поля используйте {{<Имя>.<Параметр>}}\n " +
            "Поддерживаются логическая функция {% if <Условие> %}...{% endif %} \n " +
            "Поддерживается цикл {% for [<Ключ>,] <Значение>  in <Переменная> %}, где переменная, например values у select (mysel.values) \n " +
            "!!!ВНИМАНИЕ!!! PHP внутри шаблонов не поддерживается!\n -->\n\n  {{submit}}\n{% endform %}");
        return false;
    })
    $('#selectTemplate').on('click', function(){
        editor.setValue("");
        $('.addTemplate').show();
        $('#newTemplate').slideUp();
        $('.templateSelect').slideDown();
        return false;
    })






})

$.extendsField = function() {
    $('.extendsField').off('change');
    $('.extendsField').on('change', function(){

        console.log('test')
        var form = $(this).parent().parent().parent().parent().parent().parent()
        data = {
            extends: $(this).val()
        }
        $.ajax({
            type:'POST',
            data:data,
            url: 'tools/get_field.php',
            success:function(output){
                form.replaceWith(output);
                $.extendsField();
            }
        })
        return false;
    })
}