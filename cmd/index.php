<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/design/bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="/design/bootstrap/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="/design/bootstrap/js/bootstrap.min.js"></script>
    <script src="/design/js/generator.js"></script>
    <script src="/design/ace-builds/src-min-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
    <style type="text/css">
        #editor {
            height: 500px;
        }

        .ace_editor {
            height: 500px;
        }
    </style>
</head>
<body>
<div class="row">

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Forms by WikedWolf</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="generate.php">Генерировать форму</a></li>

            </ul>
        </div>
    </nav>

    <div class="col-sm-2"></div>
    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Добро пожаловать в командную панель модуля "Формы"</h3>
            </div>
            <div class="panel-body">
                Данный модуль разработан дабы облегчить и без того нелегкую жизнь разработчика Web-сайтов.<br>
                В модуль включено множество повседневно-необходимых функций. Например отправка писем после отправки формы, запись данных формы в БД, обработчик яндекс-целей и.т.д.<br>
                Модуль легко интегрируется в любую систему. Для интеграции необходимо провести первоначальную настройку модуля, и сгенерировать YML файл настройки формы.<br>
                Используя командную панель вы сможете в два счета сделать новую форму, создать для нее шаблон и подключить к ней нужные бэкэнды.

            </div>
        </div>
    </div>
</div>
</body>
</html>