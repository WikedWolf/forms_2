<?php
$forms = require "../system/dicts/Forms.php";
$fieldsTpl = require "../system/dicts/Fields.php";
$typesArr = require "../system/dicts/Types.php";
$rulesArr = require "../system/dicts/Rules.php";
$extendsFields = '';

foreach ($fieldsTpl as $key => $tpl) {

    $extendsFields .= "<option $selected value='$key'>{$tpl['title']}</option>";
}


$formsOptions = '';

foreach ($forms as $id => $form) {
    $formsOptions .= "<option value='$id'>$id</option>";
}
$code = "";


$templatesArr = scandir('../templates');
$templates = "";
foreach ($templatesArr as $key => $template_name) {
    if (!is_dir('../templates/' . $template_name)) {
        $template_name = str_replace('.html', '', $template_name);
        $templates .= "<option value='$template_name'>$template_name</option>";
    }
}

$backends = "";
$backendsArr = scandir('../system/backends');
foreach ($backendsArr as $backend) {
    if (!is_dir('../system/backends/' . $backend)) {
        $backend = str_replace('.php', '', $backend);
        $backends .= "<label><input id='$backend' class='thumbler' type='checkbox' name='backend[]' value='$backend]'> $backend </label><br />";
    }
}


?>
<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/design/bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="/design/bootstrap/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="/design/bootstrap/js/bootstrap.min.js"></script>
    <script src="/design/js/generator.js"></script>
    <script src="/design/ace-builds/src-min-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
    <style type="text/css">
        #editor {
            height: 500px;
        }

        .ace_editor {
            height: 500px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Forms by WikedWolf</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="./">Home</a></li>
            <li class="active"><a href="generate.php">Генерировать форму</a></li>

        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Генерировать файл YML формы</h3>
            </div>
            <div class="panel-body">

                <form action="." method="post" class="form-horizontal">
                    <dl>
                        <dd><input class="form-control " type="text" name="" value="" placeholder="Идентификатор формы">
                        </dd>
                    </dl>


                    <dl>
                        <dd><label><input id="method" class="thumbler" type="checkbox" name="" value="" placeholder="">
                                Отключить AJAX</label></dd>
                    </dl>

                    <dl id="methodBox" style="display:none;">
                        <dt>Метод</dt>
                        <dd>
                            <label><input type="radio" name="method" value=""> POST</label>
                            <label><input type="radio" name="method" value=""> GET</label>
                        </dd>
                    </dl>


                    <dl>
                        <dt>Наследовать от</dt>
                        <dd>
                            <select id="extendsForm" class="form-control">
                                <option value=''>Не наследовать</option>
                                <?php echo $formsOptions; ?>
                            </select>
                        </dd>
                    </dl>


                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div id="formFields" class="col-sm-10">
                            <h3>Поля формы</h3>

                            <div class="noneFields"></div>
                        </div>
                        <div class="col-sm-1"></div>
                    </div>


                    <div class="text-center">
                        <button class="btn btn-primary" id="addField"><span class="glyphicon glyphicon-plus"></span>
                            Добавить поле
                        </button>
                    </div>
                    <dl>
                        <dt>Шаблон</dt>
                        <dd>
                        <dd>
                            <div style="width:40%" class="input-group">
                                <select class="form-control templateSelect">
                                    <option value=''>Не выбран</option>
                                    <?php echo $templates; ?>
                                </select>
                    <span class="input-group-btn">
                        <button class="addTemplate btn btn-primary"><span class="glyphicon glyphicon-plus"></span>
                            Создать
                        </button>
                    </span>
                            </div>
                            <div id="newTemplate" style="display:none">
                                <h2>Добавленеи нового шаблона</h2>

                                <div style="width:30%;" class="input-group">
                                    <input placeholder="Название" type="text" name="new_template_name" id="templateName"
                                           class="form-control">
                                    <span class="input-group-addon">.html</span>
                                </div>
                                <br>
                                <textarea placeholder="Код шаблона" class="form-control" id="editor"></textarea>

                                <div class="btn-group">
                                    <button id="saveTemplate" class="btn btn-success">Сохранить шаблон</button>
                                    <button id="clearTemplate" class="btn btn-danger">Очистить</button>
                                    <button id="selectTemplate" class="btn btn-primary">Выбрать готовый шаблон</button>
                                </div>
                            </div>
                        </dd>
                        </dd>
                    </dl>


                    <dl>
                        <dt>Обработчики</dt>
                        <dd>
                            <?php echo $backends; ?>
                            <label><input id="yandex" class="thumbler" type="checkbox" name="" value="" placeholder="">
                                Яндекс Цель</label>
                        </dd>
                    </dl>


                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div id="formFields" class="col-sm-10">


                            <dl id="BitrixAddElementBox" class="backend" style="display: none;">
                                <dt>Добавить в 1с-Битррикс</dt>
                                <dd>
                                    <input class="form-control" type="text" name="" value="" placeholder="ID Инфоблока">

                                </dd>
                            </dl>

                            <dl id="SendEmailBox" class="backend" style="display: none;">
                                <dt>Отправить EMAIL</dt>
                                <dd>
                                    <input class="form-control" type="text" name="" value="" placeholder="Кому">
                                    <input class="form-control" type="text" name="" value="" placeholder="От кого">
                                    <input class="form-control" type="text" name="" value=""
                                           placeholder="Имя отправителя">
                                    <input class="form-control" type="text" name="" value=""
                                           placeholder="Тема сообщения">
                                    <input class="form-control" type="text" name="" value="" placeholder="Шаблон">
                                </dd>
                            </dl>

                            <dl id="SendSmsBox" class="backend" style="display: none;">
                                <dt>Отправить SMS</dt>
                                <dd>
                                    <input class="form-control" type="text" name="" value="" placeholder="От кого">
                                    <input class="form-control" type="text" name="" value="" placeholder="Кому">
                                    <input class="form-control" type="text" name="" value=""
                                           placeholder="Текст сообщения">
                                </dd>
                            </dl>
                            <dl id="writeDbBox" class="backend" style="display: none;">
                                <dt>Добавить в базу данных</dt>
                                <dd>
                                    <input class="form-control" type="text" name="" value="" placeholder="Имя таблицы">

                                </dd>
                            </dl>
                            <dl id="yandexBox" style="display:none;">
                                <dt>Яндекс</dt>
                                <div class="input-group">
                                    <input style="width:40%" class="form-control" type="text" name="" value=""
                                           placeholder="ID счетчика">
                                    <input style="width:30%" class="form-control" type="text" name="" value=""
                                           placeholder="GoalID">
                                </div>
                            </dl>
                        </div>
                        <div class="col-sm-1"></div>
                    </div>

                    <dl>
                        <dd class="text-center">
                            <button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Сгенерировать
                            </button>
                        </dd>
                    </dl>
                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-2"></div>
</div>
</body>
</html>