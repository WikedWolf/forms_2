<?php
$formId = $_POST['form_id'];
$forms = require "../../system/dicts/Forms.php";

$form = $forms[$formId];

$fieldsTpl = require "../../system/dicts/Fields.php";
$typesArr = require "../../system/dicts/Types.php";
$rulesArr = require "../../system/dicts/Rules.php";
$fields = '<h3>Поля формы</h3>';
foreach ($form['fields'] as $id=>$field) {
    $extends = '';
    $extendsField = false;
    foreach ($fieldsTpl as $key=>$tpl){
        if (isset($field['extends']) && $field['extends'] == $key) {
            $selected = 'selected';
            $extendsField = $tpl;
        }else{
            $selected = '';

        }
        $extends .= "<option $selected value='$key'>{$tpl['title']}</option>";
    }



    $title = isset($field['title']) ? $field['title'] : $extendsField['title'];
    $help_text = isset($field['help_text']) ? $field['help_text'] : $extendsField['help_text'];
    $type = isset($field['type']) ? $field['type'] : $extendsField['type'];
    $rule= isset($field['rule']) ? $field['rule'] : $extendsField['rule'];

    if (isset($field['required'])){
        if ($field['required']) {
            $required = 'checked';
        }else{
            $required = '';
        }

    }elseif(isset($extendsField['required'])){
        if ($extendsField['required']) {
            $required = 'checked';
        }else{
            $required = '';
        }
    }else{
        $required = '';
    }

    $rules = '';
    foreach($rulesArr as $key=>$val) {
        if ($extendsField['rule'] && $extendsField['rule'] == $key){
            $selected = 'selected';
        }else{
            $selected = '';
        }
        $rules .= "<option $selected value='$key'>$val</option>";
    }


    $types = '';





    $types .= "<optgroup label='Buttons'>";
    foreach ($typesArr['buttons'] as $key=>$value) {
        if ($extendsField['type'] && $extendsField['type'] == $key){
            $selected = 'selected';
        }else{
            $selected = '';
        }
        $types .= "<option $selected value='$key'>$value</option>";
    }
    $types .= "</optgroup>";



    $types .= "<optgroup label='Inputs'>";
    foreach ($typesArr['inputs'] as $key=>$value) {
        if ($extendsField['type'] && $extendsField['type'] == $key){
            $selected = 'selected';
        }else{
            $selected = '';
        }
        $types .= "<option $selected value='$key'>$value</option>";
    }
    $types .= "</optgroup>";



    foreach ($typesArr['other'] as $key=>$value) {
        if ($extendsField['type'] && $extendsField['type'] == $key){
            $selected = 'selected';
        }else{
            $selected = '';
        }
        $types .= "<option $selected value='$key'>$value</option>";
    }


    $fields .= <<<EOF
 <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <dl>
                                        <dt>Имя (name)</dt>
                                        <dd><input value="$id" type="text" class="form-control"></dd>
                                    </dl>
                                </div>
                                <div class="col-sm-3">
                                    <dl>
                                        <dt>Наследовать от (extends)</dt>
                                        <dd>
                                            <select class="form-control extendsField">
                                                <option value=''>Не наследовать</option>
                                                <?php echo $extends; ?>
                                            </select>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="col-sm-3">
                                    <dl>
                                        <dt>Тип (type)</dt>
                                        <dd>
                                            <select class="form-control">
                                                <option value=''>Не выбран</option>
                                                $types
                                            </select>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="col-sm-3">
                                    <dl>
                                        <dt>Правило валидации (rule)</dt>
                                        <dd>
                                            <select class="form-control">
                                                <option value=''>Без валидации</option>
                                                $rules
                                            </select>

                                            <label><input  type="checkbox"  value="1"> Обязательное (required)</label>
                                        </dd>
                                    </dl>
                                </div>
                            </div>




                            <div class="row">
                                <div class="col-sm-6">

                                    <dl>
                                        <dt>Заголовок (title)</dt>
                                        <dd><input type="text" class="form-control" value="$title"></dd>
                                    </dl>
                                </div>
                                <div class="col-sm-6">
                                    <dl>
                                        <dt>Вспомогательный текст (help_text)</dt>
                                        <dd><input type="text" class="form-control" value="$help_text"></dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
EOF;
}

$res = [
    'form' => $form,
    'fields' => $fields
];
echo json_encode($res);die;

//'email' => [
//    'title' => 'Email',
//    'help_text' => 'Вводите настоящий Email',
//    'type' => 'text',
//    'required' => true,
//    'rule' => 'email',
//    'attrs' => [
//        'placeholder'=> 'Введите ваш адрес электронной почты',
//    ],
//],