<?php

$ext = $_POST['extends'];

    $fieldsTpl = require "../../system/dicts/Fields.php";
    $typesArr = require "../../system/dicts/Types.php";
    $rulesArr = require "../../system/dicts/Rules.php";

    $extends = '';
    $extendsField = false;
    foreach ($fieldsTpl as $key=>$tpl){
        if ( $ext == $key) {
            $selected = 'selected';
            $extendsField = $tpl;
        }else{
            $selected = '';

        }
        $extends .= "<option $selected value='$key'>{$tpl['title']}</option>";
    }

    if(isset($extendsField['required'])){
        if ($extendsField['required']) {
            $required = 'checked';
        }else{
            $required = '';
        }
    }else{
        $required = '';
    }

    $rules = '';
    foreach($rulesArr as $key=>$val) {
        if ($extendsField['rule'] && $extendsField['rule'] == $key){
            $selected = 'selected';
        }else{
            $selected = '';
        }
        $rules .= "<option $selected value='$key'>$val</option>";
    }


    $types = '';





    $types .= "<optgroup label='Buttons'>";
    foreach ($typesArr['buttons'] as $key=>$value) {
        if ($extendsField['type'] && $extendsField['type'] == $key){
            $selected = 'selected';
        }else{
            $selected = '';
        }
        $types .= "<option $selected value='$key'>$value</option>";
    }
    $types .= "</optgroup>";



    $types .= "<optgroup label='Inputs'>";
    foreach ($typesArr['inputs'] as $key=>$value) {
        if ($extendsField['type'] && $extendsField['type'] == $key){
            $selected = 'selected';
        }else{
            $selected = '';
        }
        $types .= "<option $selected value='$key'>$value</option>";
    }
    $types .= "</optgroup>";



    foreach ($typesArr['other'] as $key=>$value) {
        if ($extendsField['type'] && $extendsField['type'] == $key){
            $selected = 'selected';
        }else{
            $selected = '';
        }
        $types .= "<option $selected value='$key'>$value</option>";
    }






echo <<<EOF


        <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <dl>
                                        <dt>Имя (name)</dt>
                                        <dd><input value="$ext" type="text" class="form-control"></dd>
                                    </dl>
                                </div>
                                <div class="col-sm-3">
                                    <dl>
                                        <dt>Наследовать от (extends)</dt>
                                        <dd>
                                            <select class="form-control extendsField">
                                                <option value=''>Не наследовать</option>
                                                <?php echo $extends; ?>
                                            </select>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="col-sm-3">
                                    <dl>
                                        <dt>Тип (type)</dt>
                                        <dd>
                                            <select class="form-control">
                                                <option value=''>Не выбран</option>
                                                $types
                                            </select>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="col-sm-3">
                                    <dl>
                                        <dt>Правило валидации (rule)</dt>
                                        <dd>
                                            <select class="form-control">
                                                <option value=''>Без валидации</option>
                                                $rules
                                            </select>

                                            <label><input  type="checkbox"  value="1"> Обязательное (required)</label>
                                        </dd>
                                    </dl>
                                </div>
                            </div>




                            <div class="row">
                                <div class="col-sm-6">

                                    <dl>
                                        <dt>Заголовок (title)</dt>
                                        <dd><input type="text" class="form-control" value="{$extendsField['title']}"></dd>
                                    </dl>
                                </div>
                                <div class="col-sm-6">
                                    <dl>
                                        <dt>Вспомогательный текст (help_text)</dt>
                                        <dd><input type="text" class="form-control" value="{$extendsField['help_text']}"></dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>

EOF;
